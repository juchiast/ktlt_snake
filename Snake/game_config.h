#pragma once

struct KeyBind {
	char up, down, left, right, yes, no, enter, pause, load, del;
};
KeyBind defaultKeyBind() {
	KeyBind res;
	res.up = 'w', res.down = 's', res.left = 'a', res.right = 'd';
	res.yes = 'y', res.no = 'n';
	res.enter = 13;
	res.pause = 'l';
	res.load = 't';
	res.del = 8;
	return res;
}
struct GameConfig {
	KeyBind key;
	int max_speed;
	int base_delay;
	int delay_dec;
	int food_limit;
};
GameConfig* newGameConfig() {
	auto p = new GameConfig;
	p->key = defaultKeyBind();
	p->max_speed = 4;
	p->base_delay = 70;
	p->delay_dec = 10;
	p->food_limit = 4;
	return p;
}