#pragma once
void fix_console_window();

void get_console_size(int &w, int &h);
template<typename T> inline void get_console_size(T &T) {
	get_console_size(T.x, T.y);
}

void clear_console();

void get_cursor_pos(int &x, int &y);
template<typename T> inline void get_cursor_pos(T &T) {
	get_cursor_pos(T.x, T.y);
}

void move_cursor(int x, int y);
template<typename T> inline void move_cursor(T T) {
	move_cursor(T.x, T.y);
}