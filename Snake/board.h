#pragma once
#include <cassert>
#include <cstring>
#include "vector2.h"

struct Board {
	int w, h;
	size_t size;
	Vector2 cursor;
	char *cache, *board;
};
char& cache_at(Board *board, int x, int y) {
	assert(0 <= x && x <= board->w && 0 <= y && y <= board->h);
	return board->cache[board->w * y + x];
}
char& at(Board *board, int x, int y) {
	assert(0 <= x && x < board->w && 0 <= y && y < board->h);
	return board->board[board->w * y + x];
}
Board* newBoard(int game_size_w, int game_size_h) {
	int w = game_size_w + 2;
	int h = game_size_h + 2;
	Board *p = new Board;
	p->w = w, p->h = h;
	p->size = w*h;
	p->cache = new char[p->size];
	p->board = new char[p->size];
	p->cursor = { 0, 0 };
	std::memset(p->cache, '.', p->size);
	std::memset(p->board, ' ', p->size);
	return p;
}
void deleteBoard(Board *board) {
	delete[] board->board;
	delete[] board->cache;
	delete board;
}